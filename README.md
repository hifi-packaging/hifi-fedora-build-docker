# hifi-fedora-build-docker

Docker image for High Fidelity Fedora builds

This is still in the early stages, but it should work.

# Usage:

## To build the container yourself:

    docker build -t hifi-fedora-build:latest .
    docker run -it hifi-fedora-build

## To use the premade image from the Docker Hub:

    docker pull daleglass/hifi-fedora-build 
    docker run -it daleglass/hifi-fedora-build

## Afterwards, inside the container:

    git clone https://github.com/highfidelity/hifi.git 
    cd hifi
    mkdir build
    cd build
    cmake .. -DOpenGL_GL_PREFERENCE:STRING=GLVND
    make

# Possible issues

If the build fails, a possible problem is that some versions of Docker forbid the statx syscall, used by qt5 builds. In this case, run it like this:

    docker run --security-opt seccomp=statx_fixed.json daleglass/hifi-fedora-build

On Fedora, Docker must be run with sudo in the default configuration.

