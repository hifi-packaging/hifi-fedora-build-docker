FROM fedora:latest
MAINTAINER Dale Glass <daleglass@gmail.com>

# This is a work in progress. The final intention is to build RPM packages of HiFi.
#
# Notes:
#
# Build fails on Fedora 30 due to statx calls failing with EPERM. 
# This is a docker issue: https://github.com/moby/moby/pull/36417
# Can be worked around by:
#     docker run --security-opt seccomp=statx_fixed.json
#
# To use strace inside the container, run with:
#     docker run --cap-add SYS_PTRACE



RUN dnf update -y

# Tools absolutely needed to build at all
RUN dnf install -y gcc gcc-c++ cmake git python qt5-devel qt5-qtwebengine-devel openssl-devel

# Installed by vcpkg, we should transition to these eventually
#  * glslang[core]:x64-linux
#    hifi-host-tools[core]:x64-linux
#  * hifi-scribe[core]:x64-linux
#  * spirv-tools[core]:x64-linux
#
#
#  * bullet3[core]:x64-linux
#  * draco[core]:x64-linux
#  * etc2comp[core]:x64-linux
#  * glm[core]:x64-linux
#    hifi-client-deps[core]:x64-linux
#  * hifi-deps[core]:x64-linux
#  * nlohmann-json[core]:x64-linux
#  * nvtt[core]:x64-linux
#  * openexr[core]:x64-linux
#  * sdl2[core]:x64-linux
#  * tbb[core]:x64-linux
#  * vulkanmemoryallocator[core]:x64-linux
#  * zlib[core]:x64-linux

RUN dnf install -y glslang-devel spirv-tools-devel spirv-headers-devel spirv-tools OpenEXR-devel zlib-devel SDL2-devel SDL2_net-devel SDL2_mixer-devel SDL2_ttf-devel SDL2_gfx-devel tbb-devel nvidia-texture-tools-devel glm-devel 

# Uncertain
RUN dnf install -y mesa-vulkan-devel vulkan-loader-devel quazip-devel quazip-qt5-devel

# Additional tools not required for an actual build
# xz and pigz both can compress in parallel and are useful for making tarballs.
RUN dnf install -y xz pigz

# Missing
# * bullet3 - bullet, version 3? F30 only has 2.87
# * draco
# * etc2comp
# * SDL-mirror
# * nlohmann-json

# Missing, optional:
# * Leap Motion

# Required for Qt to find cmake during build. Bug?
ENV QT_CMAKE_PREFIX_PATH=/usr/lib64/cmake

# Optional debug tools
RUN dnf install -y strace vim


# Build with:
# mkdir build
# cd build
# cmake .. -DOpenGL_GL_PREFERENCE:STRING=GLVND

# We want to build stuff as a normal user
RUN useradd -m -u 2000  hifi
USER hifi

ENTRYPOINT /bin/bash

